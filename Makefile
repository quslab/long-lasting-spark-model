BIN=x.run
MAIN=main
GLOBAL = global
REDUCTION_sub=reduction
NVCC=nvcc
NVOPTS=-lm -O3 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_20,code=sm_20

$BIN: $(REDUCTION_sub).o $(MAIN).o
	$(NVCC) $(NVOPTS) -o $(BIN) $(REDUCTION_sub).o $(MAIN).o

$(REDUCTION_sub).o: $(REDUCTION_sub).cu $(REDUCTION_sub).h
	$(NVCC) $(NVOPTS) -dc $(REDUCTION_sub).cu

$(MAIN).o: $(MAIN).cu $(GLOBAL).h
	$(NVCC) $(NVOPTS) -dc $(MAIN).cu

clean:
	rm -rf  $(REDUCTION_sub).o $(MAIN).o $(BIN)
