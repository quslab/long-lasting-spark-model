/*
	The main parts of this code are the following:
	i) Allocate arrays for the state variables on CPU
	ii) Allocate arrays for the state variables on GPU
	iii) Define whole cell averaged state varialbes for later recordings
	iv) Create file names corresponding to different variable values (used for parameter scanning)
	v) Call Initialization kernel and set up kernel for random number generation on GPU
	vi) Call the main computing kernel for updating state variables on GPU
	vii) Update Action potential. Note that Cai, LCC and NCX are calculated on GPU for their average values, respectively
	viii) Write output files for line scan image.

	To use this code, please following the procedures below
	1) make sure you have GPUs with cuda installed. For detailes check NVIDIA website 
	"http://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/#introduction"
	2) type "make" in your terminal
	3) type "./x.run 0 -> spark_duration.txt" to run the program
	(second column of the file "spark_duration.txt" is the spark duration in the unit of ms, 
	"0" is an example input for "xratio" defined in the code line 109. 
	"xratio" controls the closed-to-open rate of RyRs. "yratio" controls the open-to-closed rate of RyRs.)	
	4) three linescan files will be outputted for visualization of linescan images. See line 212-221 for details. 
	5) long lasting sparks typically appear after 20 sec, where the system reaches the quasi steady-state.
	
	The current set of parameters here are used to generate data for Figure 3 tetracaine case. 
	Change "xratio" to 4, and "yratio" to 1 for the control case. 
	Change "xratio" to 4, and "yratio" to 20 for Figure 2 FK506 case. 
	All the data shown in the paper for the detailed model can be reproduced by this code with 
	appropriate changes in the parameters according to the main text. 
	Note that one may not obtain the exactly same data due to the randomness by using the unfixed random seed in the code.
	
	This code is used for the following paper 
	#########################################################
	"Long-lasting sparks: multi-metastability and release competition in the calcium release unit network" 

	Zhen Song1,2, Alain Karma4, James N. Weiss1,2,3, Zhilin Qu1,2,*

	1The UCLA Cardiovascular Research Laboratory and Departments of 2Medicine (Cardiology), and 3Physiology, David Geffen School of Medicine, University of California, Los Angeles, California 90095; 
	4Department of Physics, Northeastern University, Boston, MA
	#########################################################
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <curand_kernel.h>					//All subroutines
#include <curand.h>
#include <cuda.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <float.h>
#include <cfloat>
#include "global.h"
#include "reduction.h"

using namespace std;

int main(int argc, char **argv)
{
// ------------------- Input arguments ----------------
	// int CudaDevice=0;
	// if(argc>=2)
	// {
	// 	int Device=atoi(argv[2]);	// Argument #1: cuda device (default: 0)
	// 	if(Device>=0)
	// 	CudaDevice=Device;
	// }
	// cudaSetDevice(CudaDevice);

// Allocate arrays memory in CPU
	FLOAT *h_ci;		h_ci = (FLOAT *) malloc(ArraySize);							//cytosolic Ca
	FLOAT *h_cjsr;		h_cjsr = (FLOAT *) malloc(ArraySize);							//JSR Ca
	FLOAT *h_cnsr;		h_cnsr = (FLOAT *) malloc(ArraySize);								//NSR Ca
	FLOAT *h_po;		h_po = (FLOAT *) malloc(ArraySize);							//Po
	//Allocate arrays in GPU
	// For GPU random number generators
	curandStateMRG32k3a *devStates; cudaMalloc((void**)&devStates,nx*ny*nz*sizeof(curandStateMRG32k3a));
	FLOAT *d_ci; 	  	cudaMalloc((void**)&d_ci, ArraySize);
	FLOAT *d_cinext; 	cudaMalloc((void**)&d_cinext, ArraySize);
	FLOAT *d_cnsrnext;	cudaMalloc((void**)&d_cnsrnext, ArraySize);
	FLOAT *d_csnext;	cudaMalloc((void**)&d_csnext, ArraySize);
	FLOAT *d_randomi;	cudaMalloc((void**)&d_randomi, ArraySize);
	FLOAT *d_cjsr;		cudaMalloc((void**)&d_cjsr, ArraySize);
	FLOAT *d_cp;		cudaMalloc((void**)&d_cp, ArraySize);
	FLOAT *d_cs;		cudaMalloc((void**)&d_cs, ArraySize);
	FLOAT *d_cnsr;		cudaMalloc((void**)&d_cnsr, ArraySize);
	FLOAT *d_cati;		cudaMalloc((void**)&d_cati, ArraySize);
	FLOAT *d_cats;		cudaMalloc((void**)&d_cats, ArraySize);
	FLOAT *d_xire;		cudaMalloc((void**)&d_xire, ArraySize);
	FLOAT *d_xitr;		cudaMalloc((void**)&d_xitr, ArraySize);
	FLOAT *d_xicat;		cudaMalloc((void**)&d_xicat, ArraySize);
	FLOAT *d_xiup;		cudaMalloc((void**)&d_xiup, ArraySize);
	FLOAT *d_xisi;		cudaMalloc((void**)&d_xisi, ArraySize);
	FLOAT *d_xips;		cudaMalloc((void**)&d_xips, ArraySize);
	FLOAT *d_xileak;	cudaMalloc((void**)&d_xileak, ArraySize);
	FLOAT *d_xinaca;	cudaMalloc((void**)&d_xinaca, ArraySize);
	FLOAT *d_po;		cudaMalloc((void**)&d_po, ArraySize);
	int *d_lcc;			cudaMalloc((void**)&d_lcc, NLLC*ArraySize_int);
	int *d_nfu;			cudaMalloc((void**)&d_nfu, ArraySize_int);
	int *d_ntu;			cudaMalloc((void**)&d_ntu, ArraySize_int);
	int *d_nou;			cudaMalloc((void**)&d_nou, ArraySize_int);
	int *d_ncu;			cudaMalloc((void**)&d_ncu, ArraySize_int);
	int *d_nob;			cudaMalloc((void**)&d_nob, ArraySize_int);
	int *d_ncb;			cudaMalloc((void**)&d_ncb, ArraySize_int);
	int *d_nryr;		cudaMalloc((void**)&d_nryr, ArraySize_int);
	int *d_nl;			cudaMalloc((void**)&d_nl, ArraySize_int);
	int *d_n_spark;		cudaMalloc((void**)&d_n_spark, ArraySize_int);
	int *d_nsign;		cudaMalloc((void**)&d_nsign, ArraySize_int);
	FLOAT *d_t_on;		cudaMalloc((void**)&d_t_on, ArraySize);
	FLOAT *d_t_off;		cudaMalloc((void**)&d_t_off, ArraySize);
	FLOAT *d_t_spark;	cudaMalloc((void**)&d_t_spark, ArraySize);
	FLOAT *d_csp;		cudaMalloc((void**)&d_csp, ArraySize);
	FLOAT *SUMdumb_d;	cudaMalloc((void**)&SUMdumb_d, ArraySize);
	int *SUMdumb_int_d; cudaMalloc((void**)&SUMdumb_int_d, ArraySize_int);
	FLOAT *L_temp;		cudaMalloc((void**)&L_temp, ArraySize);

	// Set paramaters for geometry of computation
	dim3 threadsPerBlock(BLOCK_SIZE_X, BLOCK_SIZE_Y, BLOCK_SIZE_Z);
	dim3 numBlocks((nx+threadsPerBlock.x-1) / threadsPerBlock.x,
				   (ny+threadsPerBlock.y-1) / threadsPerBlock.y,
				   (nz+threadsPerBlock.z-1) / threadsPerBlock.z);

	// for updating Ca concentration in different compartments by swapping old and new variables with the help of these dummies.
	FLOAT *ci_buff, *cs_buff, *cnsr_buff;

	// Whole cell averaged state variables
	FLOAT cit;								//For calculating the average concentration of the cytosolic space
	FLOAT v = -80.00;   					// voltage
	FLOAT xratio;						//prefactor on Ku and Kb to change the leakiness of RYRs
	xratio = 1.0 + 1.0 * atof(argv[1]);
	FLOAT yratio=1.0;
	FLOAT dgate1=0.0;						//the voltage magnitude of left shifted activation curve in the unit of mV; 0 mV in default
	FLOAT tperiod = 1000.0;					//pacing cycle length ms
	FLOAT xnai=7.0;						//10.16;//10.16 9.38; internal Na concentration
	const FLOAT xko = 5.4;					//=3.0; //default 5.40; 2.7;   //mM    ! external K;
	FLOAT svicab = 0.381;//1.0;
	FLOAT CSQbers = 460.0;
	FLOAT TetB = 0.0;
	int num_RyR;
	FLOAT Jmaxfactor = 0.6;
	num_RyR=100;
	FLOAT Jmax = 0.0147 * Jmaxfactor;


	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$ Creat files with specific names $$$$$$$$$$$$$$$$$$$$$$
	FILE * linescan_x;
	FILE * linescan_y;
	FILE * linescan_z;
	string fid;
	fid = MakeFiles(tperiod, xratio, yratio, xko, dgate1, svicab, CSQbers, Jmaxfactor, TetB, num_RyR); //create files with specific names;
	if(LINESCAN)
	{
		string fid_LSx, fid_LSy, fid_LSz;
		fid_LSx = "LSx_" + fid;
		fid_LSy = "LSy_" + fid;
		fid_LSz = "LSz_" + fid;
		
		linescan_x=fopen(fid_LSx.c_str(),"w");
		linescan_y=fopen(fid_LSy.c_str(),"w");
		linescan_z=fopen(fid_LSz.c_str(),"w");
	}
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

	// core simulation part
	setup_kernel<<<numBlocks, threadsPerBlock>>>(time(NULL), devStates); //time(NULL) for different trials
	// Initialization and set up kernel for random number generation
	Init<<<numBlocks, threadsPerBlock>>>(devStates, d_randomi, d_ci, d_cp, d_cs, d_cjsr,
										 d_cnsr, d_cati, d_cats, d_lcc, d_nryr, d_nfu, d_ntu, d_nou,
										 d_ncu, d_nob, d_ncb, d_csp, L_temp, xratio, d_nsign, num_RyR);
	
	FLOAT t = 0.0;							//pacing time
	while (t<nbeat*tperiod)
	{
	//updating state variables
	Compute<<<numBlocks, threadsPerBlock>>>(devStates, d_nl, d_lcc, d_cp, v,
											dt, d_xicat, d_randomi, d_cjsr, d_nryr, d_nfu, d_ntu,
											d_ncu, d_nou, d_ncb, d_nob, d_po,
											d_xire, d_xinaca, d_cs,
											xnai, d_ci, d_cnsr, d_xips,
											d_xisi, d_xitr, d_csnext, d_cinext,
											d_cnsrnext, d_xiup, d_xileak,
											d_cati, d_cats,t, tperiod, d_csp, dgate1, xratio, yratio, L_temp, d_nsign,
											d_t_on, d_t_off, d_t_spark, svicab, CSQbers, Jmax, TetB);
	ci_buff=d_cinext;
	d_cinext=d_ci;
	d_ci=ci_buff;

	cs_buff=d_csnext;
	d_csnext=d_cs;
	d_cs=cs_buff;

	cnsr_buff=d_cnsrnext;
	d_cnsrnext=d_cnsr;
	d_cnsr=cnsr_buff;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~calculate ionic currents and Cai (used for Iks)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	FLOAT ek = (1.00/frt)*log(xko/xki);                // K reversal potential
	FLOAT xina = com_xina(xnai, v, dt);
	FLOAT xikr = com_xikr(v, dt, xko, ek);
	FLOAT xiks = com_xiks(v, dt, xko, xnai, cit);
	FLOAT xik1 = com_xik1(v, xko, ek);
	FLOAT xitos = com_xitos(v, dt, ek);
	FLOAT xitof = com_xitof(v, dt, ek);
	FLOAT xinak = com_xinak(v, xko, xnai);

	// Using GPU reduction kernel to average NaCa, LCC and Cai
	FLOAT xinaca = computeReduction(nx * ny * nz, d_xinaca, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz) * 0.18 * 125.0 / 20.0;   //convert ion flow to current:  net ion flow = 1/2   calcium flow
	FLOAT xica = computeReduction(nx * ny * nz, d_xicat, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz) * 500.0 * 0.18 * Vp;      //xicatotemp has already the factor of 2
	cit = computeReduction(nx * ny * nz, d_ci, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	if (APCLAMP)
	{
		v = ClampAP(t, tperiod, APD_Clamp, Vmin_Clamp, Vmax_Clamp);
	}
	else
	{
		v = Update_AP(v, tperiod, t, dt, xina, xikr, xiks, xik1, xitos, xitof, xinak, xinaca, xica); //update voltage
	}
	xnai = Update_Nai(xnai, xina, xinak, xinaca, dt);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


	if (fmod(t,2.5)<dt && t > 5000.0)
	{
		if (LINESCAN)
		{
			gpuErrchk(cudaMemcpy(h_ci, d_ci, ArraySize, cudaMemcpyDeviceToHost));
			gpuErrchk(cudaMemcpy(h_cjsr, d_cjsr, ArraySize, cudaMemcpyDeviceToHost));
			gpuErrchk(cudaMemcpy(h_cnsr, d_cnsr, ArraySize, cudaMemcpyDeviceToHost));
			gpuErrchk(cudaMemcpy(h_po, d_po, ArraySize, cudaMemcpyDeviceToHost));
			WriteLineScanx(linescan_x, t, h_ci, h_cjsr, h_cnsr, h_po, ny/2, nz/2); //13 is the index of jy, and 5 is the index of jz
			WriteLineScany(linescan_y, t, h_ci, h_cjsr, h_cnsr, h_po, nz/2, nx/2); //13 is the index of jy, and 5 is the index of jz
			WriteLineScanz(linescan_z, t, h_ci, h_cjsr, h_cnsr, h_po, nx/2, ny/2); //13 is the index of jy, and 5 is the index of jz
		}
	}

	t=t+dt;
	}

	if (LINESCAN)
	{
		fclose(linescan_x);
		fclose(linescan_y);
		fclose(linescan_z);
	}
	
	cudaFree(devStates);
	cudaFree(d_cinext);
	cudaFree(d_csnext);
	cudaFree(d_cnsrnext);
	cudaFree(d_ci);
	cudaFree(d_cs);
	cudaFree(d_cp);
	cudaFree(d_cjsr);
	cudaFree(d_cnsr);
	cudaFree(d_cati);
	cudaFree(d_cats);
	cudaFree(d_lcc);
	cudaFree(d_nryr);
	cudaFree(d_nfu);
	cudaFree(d_ntu);
	cudaFree(d_nou);
	cudaFree(d_nob);
	cudaFree(d_ncu);
	cudaFree(d_ncb);
	cudaFree(d_xire);
	cudaFree(d_xitr);
	cudaFree(d_xicat);
	cudaFree(d_xiup);
	cudaFree(d_xisi);
	cudaFree(d_xips);
	cudaFree(d_xileak);
	cudaFree(d_xinaca);
	cudaFree(d_po);
	cudaFree(d_n_spark);
	cudaFree(d_csp);
	
	cudaFree(SUMdumb_d);
	cudaFree(SUMdumb_int_d);
	cudaFree(L_temp);
	cudaFree(d_t_on);
	cudaFree(d_t_off);
	cudaFree(d_t_spark);
	cudaFree(d_nsign);
	
	free(h_ci);
	free(h_po);
	free(h_cjsr);
	free(h_cnsr);

	return 0;

}

FLOAT com_xina(FLOAT xnai, FLOAT v, FLOAT dt)
{
	static FLOAT xm=0.0010;  // sodium m-gate
	static FLOAT xh=1.00;    // sodium h-gate
	static FLOAT xj=1.00;    // soium  j-gate=

	FLOAT ena;
	//				xnai=1.0*78.0/(1.0 + 10.0*sqrt(tperiod/1000.0));
	ena= (1.0/frt)*log(xnao/xnai);        // sodium reversal potential

	FLOAT am;
	am = 0.32*(v+47.13)/(1.0-exp(-0.1*(v+47.13)));
	FLOAT bm;
	bm = 0.08*exp(-v/11.0);

	FLOAT ah,bh,aj,bj;

	if(v < -40.0)
	{
		ah = 0.135*exp((80.0+v)/(-6.8));
		bh = 3.56*exp(0.079*v)+310000.0*exp(0.35*v);
		aj=(-127140.0*exp(0.2444*v)-0.00003474*exp(-0.04391*v))*((v+37.78)/(1.0+exp(0.311*(v+79.23))));
		bj=(0.1212*exp(-0.01052*v))/(1.0+exp(-0.1378*(v+40.14)));
		//aj=ah; //make j just as h
		//bj=bh; //make j just as h
	}
	else
	{
		ah=0.00;
		bh=1.00/(0.130*(1.00+exp((v+10.66)/(-11.10))));
		aj=0.00;
		bj=(0.3*exp(-0.00000025350*v))/(1.0 + exp(-0.10*(v+32.00)));
		//aj=ah; //make j just as h
		//bj=bh; //make j just as h
	}

	FLOAT tauh;
	tauh=1.00/(ah+bh);
	FLOAT tauj;
	tauj=1.00/(aj+bj);
	FLOAT taum;
	taum=1.00/(am+bm);

	FLOAT gna=12.00;                                               // sodium conductance (mS/micro F)
	FLOAT xina = gna*xh*xj*xm*xm*xm*(v-ena);
	FLOAT xhi,xji,xmi;
	xhi=ah/(ah+bh);
	xji=aj/(aj+bj);
	xmi=am/(am+bm);
	xh = xhi-(xhi-xh)*exp(-dt/tauh);
	xj = xji-(xji-xj)*exp(-dt/tauj);
	xm = xmi-(xmi-xm)*exp(-dt/taum);

	return xina;
}

FLOAT com_xikr(FLOAT v, FLOAT dt, FLOAT xko, FLOAT ek)
{
	//  -------------- Ikr following Shannon  ------------------
	static FLOAT xr=0.00;    // ikr gate variable
	// FLOAT ek;
	// ek = (1.00/frt)*log(xko/xki);                // K reversal potential

	FLOAT gss;
	gss=sqrt(xko/5.40);
	FLOAT xkrv1;
	xkrv1=0.001380*(v+7.00)/( 1.0-exp(-0.123*(v+7.00))  );
	FLOAT xkrv2;
	xkrv2=0.000610*(v+10.00)/(exp( 0.1450*(v+10.00))-1.00);
	FLOAT taukr;
	taukr=1.00/(xkrv1+xkrv2);

	FLOAT xkrinf;
	xkrinf=1.00/(1.00+exp(-(v+50.00)/7.50));

	FLOAT rg;
	rg=1.00/(1.00+exp((v+33.00)/22.40));

	FLOAT gkr=0.0078360;  // Ikr conductance
	FLOAT xikr;
	xikr=svikr*gkr*gss*xr*rg*(v-ek);

	xr = xkrinf-(xkrinf-xr)*exp(-dt/taukr);
	return xikr;
// -----------------------------------------------------------
}

FLOAT com_xiks(FLOAT v, FLOAT dt, FLOAT xko, FLOAT xnai, FLOAT cit)
{
	// ----- Iks modified from Shannon, with new Ca dependence
//------------
	static FLOAT xs1=0.0040; // iks gate variable
	static FLOAT xs2=0.0040;
	FLOAT prnak=0.0183300;

	// FLOAT qks_inf;
	// qks_inf=0.60*(1.0*cit);                                  //should use cs in each unit      used 10*ci instead of submembrane (?) for simplicity
	// qks_inf=0.60*(1.0*1.0);
	FLOAT qks=1.0+0.8/(1+(0.5/cit)*(0.5/cit)*(0.5/cit));
	// qks=1.0+0.8/(1+(0.5/0.4)*(0.5/0.4)*(0.5/0.4));
	// FLOAT tauqks=1000.00;

	FLOAT eks;
	eks=(1.00/frt)*log((xko+prnak*xnao)/(xki+prnak*xnai));
	FLOAT xs1ss;
	xs1ss=1.0/(1.0+exp(-(v-1.500)/16.700));
	// FLOAT xs2ss;
	// xs2ss=xs1ss;

	FLOAT tauxs;
	tauxs=1.00/(0.0000719*(v+30.00)/(1.00-exp(-0.1480*(v+30.0)))+0.0001310*(v+30.00)/(exp(0.06870*(v+30.00))-1.00));
	FLOAT tauxs2;
	tauxs2=4.0*tauxs;
	FLOAT gksx=0.2000; // Iks conductance
	FLOAT xiks;
	xiks = sviks*gksx*qks*xs1*xs2*(v-eks);

	xs1=xs1ss-(xs1ss-xs1)*exp(-dt/tauxs);
	xs2=xs1ss-(xs1ss-xs2)*exp(-dt/tauxs2);
	// qks=qks+dt*(qks_inf-qks)/tauqks;
	return xiks;
}

FLOAT com_xik1(FLOAT v, FLOAT xko, FLOAT ek)
{
	//-----------------------------------------------
//      ------  Ik1 following Luo-Rudy formulation (from Shannon model)
	FLOAT gkix=0.600; // Ik1 conductance
	FLOAT gki;
	gki=gkix*(sqrt(xko/5.4));
	FLOAT aki;
	aki=1.02/(1.0+exp(0.2385*(v-ek-59.215)));
	FLOAT bki;
	bki=(0.49124*exp(0.08032*(v-ek+5.476))+exp(0.061750*(v-ek-594.31)))/(1.0+exp(-0.5143*(v-ek+4.753)));
	FLOAT xkin;
	xkin=aki/(aki+bki);
	FLOAT xik1;
	xik1=svik1*gki*xkin*(v-ek);
	return xik1;
}

FLOAT com_xitos(FLOAT v, FLOAT dt, FLOAT ek)
{
	//---------------------------------
// ------- Ito slow following Shannon et. al. 2005 ------------
	static FLOAT xtos=0.010; // ito slow activation
	static FLOAT ytos=1.00;  // ito slow inactivation
	FLOAT rt1;
	rt1=-(v+3.0)/15.00;
	FLOAT rt2;
	rt2=(v+33.5)/10.00;
	FLOAT rt3;
	rt3=(v+60.00)/10.00;
	FLOAT xtos_inf;
	xtos_inf=1.00/(1.0+exp(rt1));
	FLOAT ytos_inf;
	ytos_inf=1.00/(1.00+exp(rt2));

	FLOAT rs_inf;
	rs_inf=1.00/(1.00+exp(rt2));

	FLOAT txs;
	txs=9.00/(1.00+exp(-rt1)) + 0.50;
	FLOAT tys;
	tys=3000.00/(1.0+exp(rt3)) + 30.00;

	FLOAT gtos=0.040; // ito slow conductance

	FLOAT xitos;
	xitos=svitos*gtos*xtos*(ytos+0.50*rs_inf)*(v-ek); // ito slow

	xtos = xtos_inf-(xtos_inf-xtos)*exp(-dt/txs);
	ytos = ytos_inf-(ytos_inf-ytos)*exp(-dt/tys);
	return xitos;
}

FLOAT com_xitof(FLOAT v, FLOAT dt, FLOAT ek)
{
	//----------------------------------------------------------
// --------- Ito fast following Shannon et. al. 2005 -----------
	static FLOAT xtof=0.020; // ito fast activation
	static FLOAT ytof=0.80;  // ito slow inactivation
	FLOAT rt1;
	rt1=-(v+3.0)/15.00;
	FLOAT rt2;
	rt2=(v+33.5)/10.00;
	FLOAT xtos_inf;
	xtos_inf=1.00/(1.0+exp(rt1));
	FLOAT ytos_inf;
	ytos_inf=1.00/(1.00+exp(rt2));

	FLOAT xtof_inf;
	xtof_inf=xtos_inf;
	FLOAT ytof_inf;
	ytof_inf=ytos_inf;

	FLOAT rt4;
	rt4=-(v/30.00)*(v/30.00);
	FLOAT rt5;
	rt5=(v+33.50)/10.00;
	FLOAT txf;
	txf=3.50*exp(rt4)+1.50;
	FLOAT tyf;
	tyf=20.0/(1.0+exp(rt5))+20.00;

	FLOAT gtof=0.10;  //! ito fast conductance

	FLOAT xitof;
	xitof=gtof*xtof*ytof*(v-ek);

	xtof = xtof_inf-(xtof_inf-xtof)*exp(-dt/txf);
	ytof = ytof_inf-(ytof_inf-ytof)*exp(-dt/tyf);
	return xitof;
}

FLOAT com_xinak(FLOAT v, FLOAT xko, FLOAT xnai)
{
	//      -------  Inak (sodium-potassium exchanger) following Shannon
//        --------------

	FLOAT xkmko=1.90;//1.50; // these are Inak constants adjusted to fit
	//                // the experimentally measured dynamic restitution
	//                    curve
	FLOAT xkmnai=12.00;
	FLOAT xibarnak=1.5000;
	FLOAT hh=1.00;  // Na dependence exponent default=1.00

	FLOAT sigma;
	sigma = (exp(xnao/67.30)-1.00)/7.00;
	FLOAT fnak;
	fnak = 1.00/(1.0+0.1245*exp(-0.1*v*frt)+0.0365*sigma*exp(-v*frt));
	FLOAT xinak;
	xinak = svinak*xibarnak*fnak*(1.0/(1.0+pow((xkmnai/xnai),hh)))*xko/(xko+xkmko);
	return xinak;
}

FLOAT Update_AP(FLOAT v, FLOAT tperiod, FLOAT t, FLOAT dt, FLOAT xina, FLOAT xikr, FLOAT xiks, FLOAT xik1, FLOAT xitos, FLOAT xitof, FLOAT xinak, FLOAT xinaca, FLOAT xica)
{
	//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& previous currents from UCLA model

	FLOAT stim;

	//! -------   stimulation -----------------------------

	if(fmod(t,tperiod) < 1.5)			stim = 40.0;	//80
	else							stim= 0.0;


	//! --------  dV/dt ------------------------------------

	FLOAT dvh;
	dvh=-(xina+xik1+xikr+xiks+xitos+xitof+xinaca+xica+xinak) + stim; //0.5 0.5


	v += dvh*dt;
	return v;
}

FLOAT ClampAP(FLOAT t, FLOAT T, FLOAT APD, FLOAT Vmin, FLOAT Vmax)
{
	// FLOAT T = tperiod;
	FLOAT clampv;
	if (APD==0)
	{
		const FLOAT a = 2.0 / 3.0 * 1000.0;
		FLOAT x = a / (a + T);
		int m = static_cast<int>(t / T);
		if (m * T + x * T > t)
		{
			if (chudin)	clampv = Vmin+(Vmax-Vmin)*sqrt(1-((t-m*T)/x/T)*((t-m*T)/x/T));
			else	clampv = Vmax;

		}
		else
		{
			clampv = Vmin;
		}
	}
	else
	{
		FLOAT x = APD / T;
		int m = static_cast<int>(t / T);
		if (m * T + x * T > t)
		{
			if (chudin)	clampv = Vmin+(Vmax-Vmin)*sqrt(1-((t-m*T)/x/T)*((t-m*T)/x/T)); //chudin's experiments;
			else	clampv=Vmax; //static clamp
		}
		else
		{
			clampv=Vmin;
		}
	}
	return clampv;
}

FLOAT Update_Nai(FLOAT xnai, FLOAT xina, FLOAT xinak, FLOAT xinaca, FLOAT dt)
{
	//! --------------------------------------------------
	FLOAT wcainv;
	wcainv=1.0/50.0;      //! conversion factor between pA to micro //! amps/ micro farads
	FLOAT conv = 0.18*12500.0;  //conversion from muM/ms to pA    (includes factor of 2 for Ca2+)
	FLOAT xrr;
	xrr=(1.0/wcainv/conv)/1000.0; // note: sodium is in m molar
	//so need to divide by 1000
	FLOAT dnai;
	dnai=-xrr*(xina+3.0*xinak+3.0*xinaca);
	if(Nai_free) xnai += dnai*dt;
	else xnai=Nai_Clamp;
	return xnai;
}

void Whole_Cell_Ave_Variables(FLOAT *SUMdumb_d, FLOAT *d_cs, FLOAT *d_cp,
							  FLOAT *d_cjsr, FLOAT *d_cnsr,
							  FLOAT *d_xire, FLOAT *d_po,
							  FLOAT *d_xiup, FLOAT &cproxit, FLOAT &csubt,
							  FLOAT &cjsrt, FLOAT &cnsrt, FLOAT &poto, FLOAT &xiupto,
							  FLOAT &xireto)
{
	csubt = computeReduction(nx * ny * nz, d_cs, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	cproxit = computeReduction(nx * ny * nz, d_cp, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	cjsrt = computeReduction(nx * ny * nz, d_cjsr, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	cnsrt = computeReduction(nx * ny * nz, d_cnsr, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	xireto = computeReduction(nx * ny * nz, d_xire, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	xiupto = computeReduction(nx * ny * nz, d_xiup, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
	poto = computeReduction(nx * ny * nz, d_po, SUMdumb_d) / static_cast<FLOAT>(nx * ny * nz);
}

string MakeFiles(FLOAT tperiod, FLOAT xratio, FLOAT yratio, FLOAT xko, FLOAT dgate1, FLOAT svicab, FLOAT CSQbers, FLOAT Jmaxfactor, FLOAT TetB, FLOAT num_RyR)
{
	ostringstream fidtemp;

	if (APCLAMP)
	{
		fidtemp << "_CLAMPVM";
	}
	else
	{
		fidtemp << "_FREEVM";
	}
	if(Nai_free)
	{
		fidtemp << "_freeNai";
	}
	else
	{
		fidtemp << "_fixNai" << Nai_Clamp;
	}
	fidtemp
		   << "_xratio" << xratio
		   << "_yratio" << yratio
		   << "_Cext" << Cext
		   << "_Vp" << Vpfactor
		   << "_Vjsr" << Vjsrfactor
		   << "_Jmax" << Jmaxfactor
		   << "_svicab" << svicab
		   << "_numRyR" << num_RyR;
	string fnametrial;
	fnametrial = "Tetracaine_cj1mM"
				+ fidtemp.str()
				+ ".txt";
	return fnametrial;
}

void WriteLineScanx(FILE * linescan_x, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jy, int jz)
{
	for (int jx =0; jx < nx; jx++)
	{
		fprintf(linescan_x, "%f %d %f %f %f %f\n",t,jx,h_ci[pos(jx,jy,jz)],h_cjsr[pos(jx,jy,jz)],h_cnsr[pos(jx,jy,jz)],h_po[pos(jx,jy,jz)]);
	}
	fprintf(linescan_x, "\n");
}

void WriteLineScany(FILE * linescan_y, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jz, int jx)
{
	for (int jy =0; jy < ny; jy++)
	{
		fprintf(linescan_y, "%f %d %f %f %f %f\n",t,jy,h_ci[pos(jx,jy,jz)],h_cjsr[pos(jx,jy,jz)],h_cnsr[pos(jx,jy,jz)],h_po[pos(jx,jy,jz)]);
	}
	fprintf(linescan_y, "\n");
}
void WriteLineScanz(FILE * linescan_z, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jx, int jy)
{
	for (int jz =0; jz < nz; jz++)
	{
		fprintf(linescan_z, "%f %d %f %f %f %f\n",t,jz,h_ci[pos(jx,jy,jz)],h_cjsr[pos(jx,jy,jz)],h_cnsr[pos(jx,jy,jz)],h_po[pos(jx,jy,jz)]);
	}
	fprintf(linescan_z, "\n");
}


__global__ void Init(curandStateMRG32k3a *state, FLOAT *randomi, FLOAT *ci,FLOAT *cp,FLOAT *cs,FLOAT *cjsr,FLOAT *cnsr,
			         FLOAT *cati,FLOAT *cats,int *lcc, int *nryr, int *nfu, int *ntu, int *nou, int *ncu,int *nob,int *ncb, FLOAT *csp,
			         FLOAT *L_temp, FLOAT xratio, int *nsign, int num_RyR)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
	if (i < nx && j < ny && k < nz)
	{

		int offset = i + j * nx + k * nx * ny;

		randomi[offset]=1.0;
		ci[offset]=ci_basal;
		cp[offset]=ci_basal;
		cs[offset]=ci_basal;
		cjsr[offset]=cjsr_basal;
		cnsr[offset]=cjsr_basal;
		cati[offset]=20.0;
		cats[offset]=20.0;
		csp[offset]=ci_basal;
		int ll;
		for(ll=0; ll<NLLC; ll++)
		{

     		lcc[ll+NLLC*offset]=2;  //Mahajan LCC

		}

		nryr[offset] = num_RyR;
		ncb[offset]=0;
		ncu[offset]=int(1.0 * nryr[offset]);
		nob[offset]=0;
		nou[offset]=0;
		ntu[offset] = 0;
		nfu[offset] = 0;
		nsign[offset] = 0;
		
		curandStateMRG32k3a localState;
		localState=state[offset];
		FLOAT unif_rand=curand_uniform_double(&localState);
		L_temp[offset] = -log(unif_rand);
	}
}




__global__ void	Compute(curandStateMRG32k3a *state, int *nl, int *lcc, FLOAT *cp, FLOAT v,
					    FLOAT dt, FLOAT *xicat, FLOAT *randomi, FLOAT *cjsr, int *nryr, int *nfu, int *ntu,
					    int *ncu, int *nou, int *ncb, int *nob, FLOAT *po, FLOAT *xire,
					    FLOAT *xinaca, FLOAT *cs, FLOAT xnai, FLOAT *ci, FLOAT *cnsr,
					    FLOAT *xips, FLOAT *xisi,FLOAT *xitr, FLOAT *cs_next, FLOAT *ci_next,
					    FLOAT *cnsr_next, FLOAT *xiup, FLOAT *xileak, FLOAT *cati, FLOAT *cats,
					    FLOAT t, FLOAT tperiod, FLOAT *csp, FLOAT dgate1, FLOAT xratio, FLOAT yratio, 
					    FLOAT *L_temp, int *nsign, FLOAT *t_on, FLOAT *t_off, FLOAT *t_spark,
						FLOAT svicab, FLOAT CSQbers, FLOAT Jmax, FLOAT TetB)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
	if (i < nx && j < ny && k < nz)
	{

		int offset = i + j * nx + k * nx * ny;

		int offsetl,offsetr,offsetu,offsetd,offseti,offseto;

		offsetl = offset - 1;
		offsetr = offset + 1;
		offsetu = offset - nx;
		offsetd = offset + nx;
		offseti = offset - nx*ny;
		offseto = offset + nx*ny;

		FLOAT dotcjsr,dotci,dotcnsr;
		FLOAT xicoupn,xicoupi;
		FLOAT xicab,ecan;
		curandStateMRG32k3a localState;
		localState=state[offset];

		FLOAT svncx;
		svncx=1.0;

		FLOAT s1pref=1.0;
		FLOAT s1t=1.0*0.00195;
		FLOAT r1=1.0*0.3;

		//Boundary conditions
		if(k==0)
		{
			offseti = offset;
		}
		if(k==nz-1)
		{
			offseto = offset;
		}
		if(j==0)
		{
			offsetu = offset;
		}
		if(j==ny-1)
		{
			offsetd = offset;
		}
		if(i==0)
		{
			offsetl = offset;
		}
		if(i==nx-1)
		{
			offsetr = offset;
		}

		int jj;
		nl[offset]=0;
		int ll;
		for (ll=0; ll<NLLC; ll++)
		{
			jj=markov(&localState,lcc[ll+NLLC*offset],cp[offset],v,dt,r1,s1t,s1pref);
			lcc[ll+NLLC*offset]=jj;
			if (jj==7) nl[offset]=nl[offset]+1;
		}
		xicat[offset]=svica*(FLOAT)(nl[offset])*lcccurrent(v,cp[offset])/randomi[offset];
		xiup[offset]=sviup*uptake(ci[offset],cnsr[offset]);
		twoState_RyR_Gillespie(&localState, cp[offset], cjsr[offset], &ncu[offset], &nou[offset], &ntu[offset], &nfu[offset], &L_temp[offset], xratio, yratio);
		po[offset]=(FLOAT)(nou[offset])/(FLOAT)(nryr[offset]);
		xire[offset]=release(po[offset], nryr[offset], cjsr[offset], cp[offset], Jmax)/randomi[offset];
		if	(xire[offset]<0) xire[offset]=0.0;
		xinaca[offset]=svncx*ncx(cs[offset]/1000.0,v,tperiod,xnai,dt,&csp[offset]);
		xileak[offset]=svileak*leak(cnsr[offset],ci[offset]);
		xips[offset]=currentdps(cp[offset],cs[offset]);
		xisi[offset]=currentdsi(cs[offset],ci[offset]);
		xitr[offset]=currenttr(cnsr[offset],cjsr[offset]);
		dotcjsr=luminal(cjsr[offset], CSQbers)*(xitr[offset]-xire[offset]*Vp*randomi[offset]/Vjsr);
		cp[offset]=cs[offset]+taup*(xire[offset]-xicat[offset]);
		
		if	(cp[offset]<0) cp[offset]=0;
		xicoupn=couplingNsr(cnsr[offset],cnsr[offsetl],cnsr[offsetr],cnsr[offsetu],cnsr[offsetd],cnsr[offseti],cnsr[offseto],xi*taunl,xi*taunl,xi*taunt,xi*taunt);
		dotcnsr=(xiup[offset]-xileak[offset])*Vi/Vnsr-xitr[offset]*Vjsr/Vnsr+xicoupn;
		xicoupi=couplingI(ci[offset],ci[offsetl],ci[offsetr],ci[offsetu],ci[offsetd],ci[offseti],ci[offseto],xi*tauil,xi*tauit);
		ecan=1/frt/2.0*log(1000.0*Cext/ci[offset]);
		xicab=svicab * gcab * (v-ecan);

		dotci=instanbuf(ci[offset])*(xisi[offset]*(Vs/Vi)-xiup[offset]+xileak[offset]-troponin(cati[offset],ci[offset])+xicoupi-1.0*xicab);

		FLOAT dotcs, xicoups;
		xicoups=couplingI(cs[offset],cs[offsetl],cs[offsetr],cs[offsetu],cs[offsetd],cs[offseti],cs[offseto],xi*tausl,xi*taust);
		dotcs=instanbuf(cs[offset])*(Vp*randomi[offset]/Vs*xips[offset]+xinaca[offset]-xisi[offset]-troponin(cats[offset],cs[offset])+0.0*xicoups);
		
		cs_next[offset]=cs[offset]+dotcs*dt;
		if	(cs_next[offset]<0) cs_next[offset]=0;
		ci_next[offset]=ci[offset]+dotci*dt;
		if	(ci_next[offset]<0) ci_next[offset]=0;
		cjsr[offset]=cjsr[offset]+dotcjsr*dt;
		
		cnsr_next[offset]=cnsr[offset]+dotcnsr*dt;
		cati[offset]=cati[offset]+troponin(cati[offset],ci[offset])*dt;
		if	(cati[offset]<0) cati[offset]=0;
		cats[offset]=cats[offset]+troponin(cats[offset],cs[offset])*dt;
		if	(cats[offset]<0) cats[offset]=0;

		// spark duration for each CRU
		if (po[offset]>0.5)
		{
			if (nsign[offset]==0) t_on[offset] = t;
			nsign[offset]=1;
		}

		if	(po[offset]<=0.001 && nsign[offset]==1 && t > 5000.0)
		{
			nsign[offset]=0;
			t_off[offset] = t;
			t_spark[offset] = t_off[offset] - t_on[offset];
			printf("%f %f %f %d %d %d\n",t_on[offset], t_spark[offset],t_off[offset], i, j, k);
		}
		state[offset]=localState;
	}
}

__global__ void	setup_kernel(unsigned long long seed,curandStateMRG32k3a *state)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;
	int k = threadIdx.z + blockIdx.z * blockDim.z;
	if (i < nx && j < ny && k < nz)
	{
		int offset = i + j * nx + k * nx * ny;
		curand_init(seed,offset,0,&state[offset]);
	}

}

__device__ FLOAT luminal(FLOAT cjsr, FLOAT CSQbers)			//luminal buffer
{
	FLOAT beta;
	FLOAT roo2;
	FLOAT mono;
	FLOAT ene;
	FLOAT enedot;
	roo2=ratedimer/(1.0+pow(kdimer/cjsr,hilldimer));
	mono=(-1.0+sqrt(1.0+8.0*CSQbers*roo2))/(4.0*roo2*CSQbers);
	ene=mono*nM+(1.0-mono)*nD;
	enedot=(nM-nD)*(-mono + 1.0/(4.0*CSQbers*mono*roo2 + 1.0))*(hilldimer/cjsr)*(1.0 - roo2/ratedimer);
	beta=1.0/(1.0 + (CSQbers*kbers*ene + enedot*cjsr*(cjsr+kbers))/((kbers+cjsr)*(kbers+cjsr)));
	return(beta);
}

__device__ void twoState_RyR_Gillespie (curandStateMRG32k3a *state, FLOAT cp, FLOAT cj, int * ncu, int * nou, int * ntu, int * nfu, FLOAT *L_temp, FLOAT xratio, FLOAT yratio)
{
	FLOAT cjth=610.0;
	FLOAT hill=10.0;

	FLOAT pku = (*ncu) * xratio * Ku * (1.0/(1.0 + pow(cjth/cj, hill))) * (cp * cp); 		//CU -> OU
	FLOAT pkuminus = (*nou) * 1.0/tauc * yratio; 	//OU -> CU

	FLOAT lammda0 = pku;
	FLOAT lammda7 = lammda0 + pkuminus;
	FLOAT unif_rand;

	FLOAT m = *L_temp - dt * lammda7;
	FLOAT t_loc;
	if (m <= 0)
	{
		t_loc = *L_temp/lammda7;
		unif_rand=curand_uniform_double(state);
		if (unif_rand < lammda0 / lammda7)	{(*ncu)--; (*nou)++;}
		else 				 				{(*nou)--; (*ncu)++;}
		pku = (*ncu) * xratio* Ku * (1.0/(1.0 + pow(cjth/cj, hill))) * (cp * cp); 		//CU -> OU
		pkuminus = (*nou) * 1.0 / tauc * yratio; 	//OU -> CU
		lammda0 = pku;
		lammda7 = lammda0 + pkuminus;
		unif_rand=curand_uniform_double(state);
		*L_temp = -log(unif_rand);
		t_loc += *L_temp/lammda7;
		while (t_loc < dt)
		{
			unif_rand=curand_uniform_double(state);
			if (unif_rand < lammda0 / lammda7)	{(*ncu)--; (*nou)++;}
			else								{(*nou)--; (*ncu)++;}
			pku = (*ncu) * xratio* Ku * (1.0/(1.0 + pow(cjth/cj, hill))) * (cp * cp); 		//CU -> OU
			pkuminus = (*nou) * 1.0 / tauc * yratio; 	//OU -> CU
			lammda0 = pku;
			lammda7 = lammda0 + pkuminus;
			unif_rand=curand_uniform_double(state);
			*L_temp = -log(unif_rand);
			t_loc += *L_temp/lammda7;
		}
		*L_temp = (t_loc - dt)*lammda7;
	}
	else
	{
		*L_temp = m;
	}
}

// Mahajan LCC
__device__ int markov(curandStateMRG32k3a *state,int i, FLOAT cpx,FLOAT v, FLOAT dt, FLOAT r1, FLOAT s1t, FLOAT s1pref)
{
	const FLOAT vth = 0.0;
	const FLOAT s6 = 4.0;
	const FLOAT taupo = 1.0;			//ms
	const FLOAT r2 = 6.0;				//6			//3			//6
	const FLOAT cat = 0.5;				//0.5		//3			//2.0
	const FLOAT cpt = 1.5;				//1.5		//6.1		//6.1
	const FLOAT xk2 = 0.0005;
	const FLOAT xk1t = 0.00413;
	const FLOAT xk2t = 0.00224;
	const FLOAT vx = -40.0;			//40
	const FLOAT sx = 3.0;
	const FLOAT tau3 = 3.0;			//ms
	const FLOAT vy = -40.0;			//40
	const FLOAT sy = 4.0;
	const FLOAT tca = 114.0;
	const FLOAT vyr = -40.0;			//40
	const FLOAT syr = 11.32;
	int jj;
	FLOAT poinf=1.0/(1.0+exp(-(v-vth)/s6));
	FLOAT alpha=poinf/taupo;
	FLOAT beta=(1.0-poinf)/taupo;
	FLOAT s2t=s1t*(r1/r2)*(xk2t/xk1t);
    FLOAT poi=1.0/(1.0+exp(-(v-vx)/sx));
	FLOAT xk3=(1.0-poi)/tau3;
	FLOAT xk3t=xk3;
	FLOAT prv=1.0-1.0/(1.0+exp(-(v-vy)/sy));
	FLOAT recov=10.0+4954.0*exp(v/15.6);
	FLOAT poix=1.0/(1.0+exp(-(v-vyr)/syr));
	FLOAT fca=1.0/(1.0+pow(cat/cpx,3.0));
	FLOAT s1=0.02*fca*s1pref;
	FLOAT xk1=0.03*fca;
	FLOAT s2=s1*(r1/r2)*(xk2/xk1);  //reversibility conditions
	FLOAT tau_ca=tca/(1.0+pow(cpx/cpt,4.0));
	FLOAT tauca=(recov-tau_ca)*prv+tau_ca;
	FLOAT tauba=(recov-450.0)*prv+450.0;
	FLOAT xk6=fca*poix/tauca;
	FLOAT xk5=(1.0-poix)/tauca;
	FLOAT xk6t=poix/tauba;
	FLOAT xk5t=(1.0-poix)/tauba;
	FLOAT xk4=xk3*(alpha/beta)*(xk1/xk2)*(xk5/xk6);
	FLOAT xk4t=xk3t*(alpha/beta)*(xk1t/xk2t)*(xk5t/xk6t);
	FLOAT ragg=curand_uniform_double(state);
	FLOAT rig = ragg/dt;

	if(i==1)
	{//1
		if(rig < beta)											jj = 2;
		else if (beta < rig && rig < beta+r1)					jj = 7;
		else if(beta+r1 < rig && rig < beta+r1+xk1t)			jj = 4;
		else if(beta+r1+xk1t < rig && rig < beta+r1+xk1t+xk1)	jj = 3;
		else													jj = 1;
	}//1

	if(i==2)
	{//2
		if(rig < xk6)											jj = 5;
		else if(xk6 < rig && rig < xk6+xk6t) 					jj = 6;
		else if(xk6+xk6t < rig && rig < xk6+xk6t+alpha) 	    jj = 1;
		else													jj = 2;
	}//2

	if(i==3)
	{//3
		if(rig < xk3)											jj = 5;
		else if(xk3 < rig && rig < xk3+xk2) 					jj = 1;
		else if(xk3+xk2 < rig && rig <xk3+xk2+s2)				jj = 7;
		else													jj = 3;
	}//3

	if(i==4)
	{//4
		if(rig < xk3t)											jj = 6;
		else if(xk3t < rig && rig < xk3t+xk2t)					jj = 1;
		else if(xk3t+xk2t < rig && rig < xk3t+xk2t+s2t)			jj = 7;
		else													jj = 4;
	}//4

	if(i==5)
	{//5
		if(rig < xk5)											jj = 2;
		else if(xk5 < rig && rig < xk5+xk4) 					jj = 3;
		else													jj = 5;
	}//5

	if(i==6)
	{//6
		if(rig < xk5t)											jj = 2;
		else if(xk5t < rig && rig < xk5t+xk4t)					jj = 4;
		else													jj = 6;
	}//6

	if(i==7)
	{//7
		if(rig < r2)											jj = 1;
		else if(r2 < rig && rig < r2+s1)						jj = 3;
		else if(r2+s1 < rig && rig < r2+s1+s1t)					jj = 4;
		else													jj = 7;

		if((r2+s1+s1t)*dt >=1.0)
		{ dt=1.0/(r2+s1+s1t);
		  printf("Warning: Time step changed to dt= %f\n", dt);
		}

	}//7
	return(jj);

}


__device__ FLOAT ncx (FLOAT cs, FLOAT v,FLOAT tperiod, FLOAT xnai, FLOAT dt, FLOAT *csp)		//Na_Ca exchanger
{
	FLOAT Inaca;
	FLOAT t1;
	FLOAT t2;
	FLOAT t3;
	FLOAT Ka;
	FLOAT za=v*Farad/xR/Temper;
	*csp=cs;
	Ka=1.0/(1.0+Kda/(*csp)*Kda/(*csp)*Kda/(*csp)); //control
	t1=Kmcai*xnao*xnao*xnao*(1.0+xnai/Kmnai*xnai/Kmnai*xnai/Kmnai);
	t2=Kmnao*Kmnao*Kmnao*(*csp)+Kmnai*Kmnai*Kmnai*Cext*(1.0+(*csp)/Kmcai);
	t3=(Kmcao+Cext)*xnai*xnai*xnai+(*csp)*xnao*xnao*xnao;
	Inaca=Ka*vnaca*(exp(eta*za)*xnai*xnai*xnai*Cext-exp((eta-1.0)*za)*xnao*xnao*xnao*(*csp))/((t1+t2+t3)*(1.0+ksat*exp((eta-1.0)*za)));
	return (Inaca);
}

__device__ FLOAT instanbuf(const FLOAT &ci)			//cytosolic buffer ; const reference does not copy variable, so the time penalty is minimum.
{
	FLOAT beta;
	FLOAT temp1;
	FLOAT temp2;
	FLOAT temp3;
	FLOAT temp4;
	temp1=Kcam*Bcam/((ci+Kcam)*(ci+Kcam));
	temp2=Ksr*Bsr/((ci+Ksr)*(ci+Ksr));
	temp3=Kmca*Bmca/((ci+Kmca)*(ci+Kmca));
	temp4=Kmmg*Bmmg/((ci+Kmmg)*(ci+Kmmg));
	beta=1.0/(1.0+temp1+temp2+temp3+temp4);
	return(beta);
}

__device__ FLOAT troponin(const FLOAT &CaT, const FLOAT &calciu)		//troponin
{
	FLOAT Itc;
	Itc=kton*calciu*(Bt-CaT)-ktoff*CaT;
	return(Itc);
 }

__device__ FLOAT currentdsi(const FLOAT &cs, const FLOAT &ci)		//diffusion from cs to ci
{
	FLOAT Idsi;
	Idsi=(cs-ci)/tausi;
	return(Idsi);
}

__device__ FLOAT uptake(const FLOAT &ci, const FLOAT &cnsr)			//uptake
{
	FLOAT Iup;
	Iup=upfactor*vup*(pow(ci/Ki,HH)-pow(cnsr/Knsr,HH))/(1.0+pow(ci/Ki,HH)+pow(cnsr/Knsr,HH));
	return(Iup);
}

__device__ FLOAT leak(const FLOAT &cnsr, const FLOAT & ci)			//leak from nsr
{
	FLOAT Ileak;
	Ileak=gleak*(cnsr-ci)*(cnsr*cnsr)/((cnsr*cnsr)+(Kjsr*Kjsr));
	return(Ileak);
 }

__device__ FLOAT currenttr(const FLOAT &cnsr, const FLOAT &cjsr)	//refilling from Nsr to Jsr
{
	FLOAT Itr;
	Itr=(cnsr-cjsr)/tautr;
	return (Itr);
}


__device__ FLOAT currentdps(const FLOAT &cp, const FLOAT &cs)		//diffusion from the proximal to cs
{
	FLOAT Idps;
	Idps=(cp-cs)/taup;
	return (Idps);
}

__device__ FLOAT couplingNsr (const FLOAT &ccc0, const FLOAT &ccc1, const FLOAT &ccc2, const FLOAT &ccc3, const FLOAT &ccc4, const FLOAT &ccc5, const FLOAT &ccc6, const FLOAT &taul_l, const FLOAT &taul_r, const FLOAT &taut_u, const FLOAT &taut_d)
{
	FLOAT Icoup;
    Icoup=(ccc1-ccc0)/taul_l+(ccc2-ccc0)/taul_r+(ccc3-ccc0)/taut_d+(ccc4-ccc0)/taut_u+(ccc5-ccc0)/taut_d+(ccc6-ccc0)/taut_u;
	return(Icoup);
}

//coupling effect from neighbours
__device__ FLOAT couplingI (const FLOAT &ccc0,  const FLOAT &ccc1, const FLOAT &ccc2, const FLOAT &ccc3, const FLOAT &ccc4, const FLOAT &ccc5, const FLOAT &ccc6, const FLOAT &taul, const FLOAT &taut)
{
	FLOAT Icoup;
	Icoup=(ccc2+ccc1-2.0*ccc0)/taul+(ccc4+ccc3-2.0*ccc0)/taut+(ccc6+ccc5-2.0*ccc0)/taut;
	return(Icoup);
}


__device__ FLOAT release(const FLOAT &po, const int &nryr, const FLOAT &cjsr, const FLOAT &cp, FLOAT Jmax)	//SR release current
{
	FLOAT Ir;
	Ir=(Jmax / 100.0) * po * nryr *(cjsr-cp)/Vp;				// /randomi[i] outside
	return (Ir);
}

__device__ FLOAT lcccurrent(const FLOAT &v, const FLOAT &cp)		//Ica
{
	FLOAT za=v*Farad/xR/Temper;
	FLOAT ica;

	if (fabs(za)<0.001)
	{
	 ica=2.0*Pca*Farad*gammai*(cp/1000.0*exp(2.0*za)-Cext);
	}
	else
	{
	 ica=4.0*Pca*za*Farad*gammai*(cp/1000.0*exp(2.0*za)-Cext)/(exp(2.0*za)-1.0);
	}
	 ica=(0.06/Vp)*(ica/Pca)/4.0;		// factor 0.5 is for testing whether cptilde=3 works with this model, because we don't want to change the whole cell Ica
	if (ica > 0.0) ica=0.0;
	return (ica);
}
