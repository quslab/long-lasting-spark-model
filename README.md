# README #

### What is this repository for? ###

* This code is used for the following paper: 

Long-lasting sparks: multi-metastability and release competition in the calcium release unit network

Zhen Song1,2, Alain Karma4, James N. Weiss1,2,3, Zhilin Qu1,2,*

1The UCLA Cardiovascular Research Laboratory and Departments of 2Medicine (Cardiology), and 3Physiology, David Geffen School of Medicine, University of California, Los Angeles, California 90095; 4Department of Physics, Northeastern University, Boston, MA

Citation: Song Z, Karma A, Weiss JN, Qu Z (2016) Long-Lasting Sparks: Multi-Metastability and Release Competition in the Calcium Release Unit Network. PLoS Comput Biol 12(1): e1004671. doi:10.1371/ journal.pcbi.1004671


### How do I get set up? ###

To use this code, please following the procedures below

* make sure you have GPUs with cuda installed. For detailes check NVIDIA website "http://docs.nvidia.com/cuda/cuda-compiler-driver-nvcc/#introduction"

* type "make" in your terminal

* type "./x.run 0 -> spark_duration.txt" to run the program (second column of the file "spark_duration.txt" is the spark duration in the unit of ms, "0" is an example input for "xratio" defined in the code line 109. "xratio" controls the closed-to-open rate of RyRs. "yratio" controls the open-to-closed rate of RyRs.)	

* three linescan files will be outputted for visualization of linescan images. See line 212-221 for details. 

* long-lasting sparks typically appear after 20 sec, where the system reaches the quasi steady-state. So please wait for enough time to observe long-lasting sparks.

* The current set of parameters here are used to generate data for Figure 3 tetracaine case. Change "xratio" to 4, and "yratio" to 1 for the control case. Change "xratio" to 4, and "yratio" to 20 for Figure 2 FK506 case. All the data shown in the paper for the detailed model can be reproduced by this code with appropriate changes in the parameters according to the main text. Note that one may not obtain the exactly same data due to the randomness by using the unfixed random seed in the code, but qualitatively the data should be very similar.

### Who do I talk to? ###
* Please feel free to email us regarding any question. Email: zsong@mednet.ucla.edu or zqu@mednet.ucla.edu