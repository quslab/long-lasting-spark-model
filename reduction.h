////////from reduction.h below
#ifndef __REDUCTION_H__
#define __REDUCTION_H__

#ifndef FLOAT
#define FLOAT double
#endif

template <class T>
void reduce(int size, int threads, int blocks, 
                 int whichKernel, T *d_idata, T *d_odata);


////////from reduction.h above

#define MAX_THREADS 512
#define MAX_BLOCKS   64

extern "C"
bool isPow2(unsigned int x)
{
  return ((x&(x-1))==0);
}


////////////////////////////////////////////////////////////////////////////////
//! Compute sum reduction on CPU
//! We use Kahan summation for an accurate sum of large arrays.
//! http://en.wikipedia.org/wiki/Kahan_summation_algorithm
//! 
//! @param data       pointer to input data
//! @param size       number of input data elements
////////////////////////////////////////////////////////////////////////////////
FLOAT reduceCPU(FLOAT *data, int size)
{
    FLOAT sum = data[0];
    FLOAT c = (FLOAT)0.0;              
    for (int i = 1; i < size; i++)
    {
        FLOAT y = data[i] - c;  
        FLOAT t = sum + y;      
        c = (t - sum) - y;  
        sum = t;            
    }
    return sum;
}

unsigned int nextPow2( unsigned int x ) {
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    return ++x;
}

#ifndef MIN
#define MIN(x,y) ((x < y) ? x : y)
#endif

////////////////////////////////////////////////////////////////////////////////
// Compute the number of threads and blocks to use for the given reduction kernel
// For the kernels >= 3, we set threads / block to the minimum of maxThreads and
// n/2. For kernels < 3, we set to the minimum of maxThreads and n.  For kernel 
// 6, we observe the maximum specified number of blocks, because each thread in 
// that kernel can process a variable number of elements.
////////////////////////////////////////////////////////////////////////////////
void getNumBlocksAndThreads(int n, int &blocks, int &threads)
{
  threads = (n < MAX_THREADS*2) ? nextPow2((n + 1)/ 2) : MAX_THREADS;
  blocks = MIN(MAX_BLOCKS, (n + (threads * 2 - 1)) / (threads * 2));
}

////////////////////////////////////////////////////////////////////////////////
// This function performs a reduction of the input data multiple times and 
// measures the average reduction time.
////////////////////////////////////////////////////////////////////////////////
template <class T>
T computeReduction(int n, T* d_idata, T* d_odata)
{
  T gpu_result = 0;
      
  int numThreads = 0, numBlocks = 0;
  getNumBlocksAndThreads(n, numBlocks, numThreads);
  reduce<T>(n, numThreads, numBlocks, 6, d_idata, d_odata);
  
  // sum partial block sums on GPU
  int s=numBlocks;
  while(s > 1) 
  {
    int threads = 0, blocks = 0;
    getNumBlocksAndThreads(s, blocks, threads);
    
    reduce<T>(s, threads, blocks, 6, d_odata, d_odata);
    
    s = (s + (threads*2-1)) / (threads*2);
  } 

  // copy final sum from device to host
  cudaMemcpy(&gpu_result, d_odata, sizeof(T), cudaMemcpyDeviceToHost);
  
  return gpu_result;
}


//////////for reduce for int
// int computeReductionINT(int n, int* d_idata, int* d_odata)
// {
//   int gpu_result = 0;
      
//   int numThreads = 0, numBlocks = 0;
//   getNumBlocksAndThreads(n, numBlocks, numThreads);
//   reduce<int>(n, numThreads, numBlocks, 6, d_idata, d_odata);
  
//   // sum partial block sums on GPU
//   int s=numBlocks;
//   while(s > 1) 
//   {
//     int threads = 0, blocks = 0;
//     getNumBlocksAndThreads(s, blocks, threads);
    
//     reduce<int>(s, threads, blocks, 6, d_odata, d_odata);
    
//     s = (s + (threads*2-1)) / (threads*2);
//   } 

//   // copy final sum from device to host
//   cudaMemcpy(&gpu_result, d_odata, sizeof(int), cudaMemcpyDeviceToHost);
  
//   return gpu_result;
// }

#endif
