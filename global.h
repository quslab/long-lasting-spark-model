/*
	There are several flags for various simulation purpose.
	1) LINESCAN = true --> output line scan files
	2) Nai_free = false --> clamp Nai
	3) APCLAMP = true --> Clamp AP
*/
#ifndef GLOBAL
#define GLOBAL
	#include <string>
	using namespace std;
	#define FLOAT double
// #################################
// ##### flags for outputing LS ####
// ##### and animation frames ######
	const bool LINESCAN = true;
// #################################
// #################################
	const bool chudin = false; const FLOAT APD_Clamp = 0; //APD = 0 default duration in chudin or specify the duration
							  const FLOAT Vmin_Clamp = -80.0; const FLOAT Vmax_Clamp = Vmin_Clamp;
	const bool Nai_free = false; const FLOAT Nai_Clamp = 12.0;
	const bool APCLAMP = true;
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// Domain size
	const int nx = 64;		//25		//65	//Number of Units in the x direction
	const int ny = 32;		//10		//27	//Number of Units in the y direction
	const int nz = 16;		//4			//11	//Number of Units in the z direction
	// CUDA block size
	const int BLOCK_SIZE_X = 8;//8;
	const int BLOCK_SIZE_Y = 8;//8;
	const int BLOCK_SIZE_Z = 4;//4;
	// Mem size
	const int ArraySize = nx*ny*nz*sizeof(FLOAT);		//Mem size for FLOAT
	const int ArraySize_int = nx*ny*nz*sizeof(int);	//Mem size for int
	//macro of array element index
	const int STRIDE = nx * ny;
	const int WIDTH = nx;
	#define pos(x,y,z)	(STRIDE * z + WIDTH * y + x)

	// universal constants
	const FLOAT Farad = 96.485;		//	C/mmol
	const FLOAT xR	= 8.314;		//	J/mol/K
	const FLOAT Temper	= 308.0;	//	308		//K
	const FLOAT frt = Farad / xR / Temper;
	const FLOAT pi = 3.1415926;

	// simulation parameters
	const int nbeat = 100;		//number of pacing beat
	const FLOAT dt = 0.01;

	// CSQN parameters for buffering and RyR kinetics
	const FLOAT nM = 15.0;
	const FLOAT nD = 35.0;
	const FLOAT ratedimer = 5000.0;
	const FLOAT kdimer = 850.0;
	const FLOAT hilldimer = 23.0;
	// const FLOAT CSQbers = 460.0;				//400.0   //460
	const FLOAT kbers = 600.0;
	const FLOAT tauc = 1.0;
	const FLOAT Ku = 0.00038;					//RyR close-unbound to open-unbound transition rate
	const FLOAT Kb = 0.00005;					//RyR close-bound to open-bound transition rate
	// luminal buffer constant
	const FLOAT Kcam=7.0;
	const FLOAT Bcam=24.0;
	const FLOAT Ksr=0.6;
	const FLOAT Bsr=47.0;
	const FLOAT Kmca=0.033;
	const FLOAT Bmca=140.0;
	const FLOAT Kmmg=3.64;
	const FLOAT Bmmg=140.0;
	// troponion constant
	const FLOAT Bt=70.0;
	const FLOAT kton=0.0327;
	const FLOAT ktoff=0.0196;
	// SERCA constant
	const FLOAT vup=0.36;				//0.3 for T=400ms, 0.36 default
	const FLOAT Ki=0.123;	//0.123 default
	const FLOAT Knsr=1700.0;			//1700 for T=400ms
	const FLOAT HH=1.787;
	const FLOAT upfactor=1.0;			//factor of SERCA increasing
	// leak current constant
	const FLOAT gleak=0.00001035;
	const FLOAT Kjsr=500.0;
	const FLOAT Pca = 11.9;		//	umol/C/ms
	const FLOAT gammai = 0.341;
	// NCX constant
	const FLOAT vnaca = 1.0 * 7.0;//(7)	//21 default
	const FLOAT Kmcai = 0.00359;
	const FLOAT Kmcao = 1.3;
	const FLOAT Kmnai = 12.3;
	const FLOAT Kmnao = 87.5;
	const FLOAT Kda = 0.00011;		//0.00011		//0.0003		//mM
	const FLOAT ksat = 0.27;	//0.27 default
	const FLOAT eta = 0.35;
	const FLOAT tauKa = 50.0;
	// Cell related constant
	const FLOAT Cext = 1.8;//5.0; //1.8		// external  Ca (mM)
	const FLOAT Vpfactor = 0.7;
	const FLOAT Vp = 0.00126*Vpfactor;			//0.00126	//Volume of the proximal space
	const FLOAT Vjsrfactor = 1.5;
	const FLOAT Vjsr = 0.02*Vjsrfactor;			//0.02 normal			//Volume of the Jsr space
	const FLOAT Vi = 0.5;							//Volume of the Local cytosolic
	const FLOAT Vs = 0.025;						//Volume of the Local submembrane space
	const FLOAT Vnsr = 0.025;						//Volume of the Local Nsr s	ace
	// const int nryr = 100.0;			//100!!!!!!!!!	//Number of Ryr channels
	const int NLLC = 8;	// Number of LCCs in each CRU
	const FLOAT taup = 0.022;			//0.022		//Diffusion time from the proximal to the submembrane
	const FLOAT tausi = 0.1;						//Diffusion time from the submembrane to the cytosolic
	const FLOAT tautr = 5.0;		//139*0.15	//5			//Diffusion time from NSR to JSR
	const FLOAT taunl = 24.0;			//60			//Longitudinal NSR
	const FLOAT taunt = 7.2;			//18			//Transverse NSR
	const FLOAT tauil = 2.32;			//5.8			//Longitudinal cytosolic
	const FLOAT tauit = 2.93;			//7.34			//Transverse cytosolic
	const FLOAT tausl = 3.4;			//8.5			//Longitudinal submembrane
	const FLOAT taust = 1.42;			//3.54			//Transverse submembrane
	const FLOAT xi = 0.7;//1000000.0;			//diffusive coupling coefficient 0.7 default.
	const FLOAT ci_basal = 0.1; //initial value
	const FLOAT cjsr_basal = 1000.0; //initial value
	const FLOAT gcab = 1.0*0.003016*50.0/0.18/12500.0; //conductance of ICab (background Ca current) in the unit of microM/ms
	const FLOAT xnao = 136.0; //Nao 136 mM
	const FLOAT xki=140.0;  //Ki mM   ! internal K
	//prefactors of ion current
	const FLOAT sviks = 1.0;
	const FLOAT svikr = 1.0;
	const FLOAT svinak = 1.0;
	const FLOAT svik1 = 1.0;
	const FLOAT svitos = 1.0;
	const FLOAT svileak = 1.0;
	const FLOAT sviup = 1.0;//6.8;
	const FLOAT svica = 0;	//0.7

	//functions to calculate INa, Ikr, Iks, Ik1, Itos, Itof, INaK
	FLOAT com_xina(FLOAT xnai, FLOAT v, FLOAT dt);
	FLOAT com_xikr(FLOAT v, FLOAT dt, FLOAT xko, FLOAT ek);
	FLOAT com_xiks(FLOAT v, FLOAT dt, FLOAT xko, FLOAT xnai, FLOAT cit);
	FLOAT com_xik1(FLOAT v, FLOAT xko, FLOAT ek);
	FLOAT com_xitos(FLOAT v, FLOAT dt, FLOAT ek);
	FLOAT com_xitof(FLOAT v, FLOAT dt, FLOAT ek);
	FLOAT com_xinak(FLOAT v, FLOAT xko, FLOAT xnai);
	//function to update AP
	FLOAT Update_AP(FLOAT v, FLOAT tperid, FLOAT t, FLOAT dt, FLOAT xina,
					 FLOAT xikr, FLOAT xiks, FLOAT xik1, FLOAT xitos,
					 FLOAT xitof, FLOAT xinak, FLOAT xinaca, FLOAT xica);
	// clamp AP (bool chudin determine the type of AP clamp)
	FLOAT ClampAP(FLOAT t, FLOAT T, FLOAT APD, FLOAT Vmin, FLOAT Vmax);
	// function to update Nai
	FLOAT Update_Nai(FLOAT xnai, FLOAT xina, FLOAT xinak, FLOAT xinaca, FLOAT dt);
	// calculate whole cell averaged state variables
	void Whole_Cell_Ave_Variables(FLOAT *SUMdumb_d, FLOAT *d_cs, FLOAT *d_cp,
								  FLOAT *d_cjsr, FLOAT *d_cnsr,
								  FLOAT *d_xire, FLOAT *d_po,
								  FLOAT *d_xiup, FLOAT &cproxit, FLOAT &csubt,
								  FLOAT &cjsrt, FLOAT &cnsrt, FLOAT &poto, FLOAT &xiupto,
								  FLOAT &xireto);
	// create files
	string MakeFiles(FLOAT tperid, FLOAT xratio, FLOAT yratio, FLOAT xko, FLOAT dgate1, FLOAT svicab, FLOAT CSQbers, FLOAT Jmaxfactor, FLOAT TetB, FLOAT num_RYR);
	// output line scan file
	void WriteLineScanx(FILE * linescan_x, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jy, int jz);
	void WriteLineScany(FILE * linescan_y, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jz, int jx);
	void WriteLineScanz(FILE * linescan_z, FLOAT t, FLOAT *h_ci, FLOAT *h_cjsr, FLOAT *h_cnsr, FLOAT *h_po, int jx, int jy);

	// GPU error checking macro
	#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
	inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
	{
	   if (code != cudaSuccess)
	   {
	      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	      if (abort) exit(code);
	   }
	}
	// luminal buffer
	__device__ FLOAT luminal(FLOAT cjsr, FLOAT CSQbers);
	// RyR gating kinetics
	__device__ void twoState_RyR_Gillespie (curandStateMRG32k3a *state, FLOAT cp, FLOAT cjsr, int * ncb, 
								int * nob, int * ntu, int * nfu, FLOAT *L_temp, FLOAT xratio, FLOAT yratio);

	// LCC gating kinetics
		__device__ int markov(curandStateMRG32k3a *state,int i, FLOAT cpx,FLOAT v, FLOAT dt,
							  FLOAT r1, FLOAT s1t, FLOAT s1pref);

	__device__ FLOAT ncx (FLOAT cs, FLOAT v,FLOAT tperiod, FLOAT xnai, FLOAT dt, FLOAT *csp);	// NaCa
	__device__ FLOAT instanbuf(const FLOAT &ci);	//cytosolic buffer
	__device__ FLOAT troponin(const FLOAT &CaT, const FLOAT &calciu);	//troponi kinetics
	__device__ FLOAT currentdsi(const FLOAT &cs, const FLOAT &ci);	//diffusion between Ci and Cs
	__device__ FLOAT uptake(const FLOAT &ci, const FLOAT &cnsr);	//SERCA from Ci to Cnsr
	__device__ FLOAT leak(const FLOAT &cnsr, const FLOAT & ci);	//Leak flux from Cnsr to Ci
	__device__ FLOAT currenttr(const FLOAT &cnsr, const FLOAT &cjsr);	//diffusion from Cnsr to Cjsr
	__device__ FLOAT currentdps(const FLOAT &cp, const FLOAT &cs);	//diffusion from Cp to Cs
	__device__ FLOAT couplingNsr (const FLOAT &ccc0, const FLOAT &ccc1, //NSR diffusion from neighboring CRUs
								   const FLOAT &ccc2, const FLOAT &ccc3,
								   const FLOAT &ccc4, const FLOAT &ccc5,
								   const FLOAT &ccc6, const FLOAT &taul_l,
								   const FLOAT &taul_r, const FLOAT &taut_u,
								   const FLOAT &taut_d);
	__device__ FLOAT couplingI (const FLOAT &ccc0,  const FLOAT &ccc1,	//Cytosolic diffusion from neighboring CRUs
								 const FLOAT &ccc2, const FLOAT &ccc3,
								 const FLOAT &ccc4, const FLOAT &ccc5,
								 const FLOAT &ccc6, const FLOAT &taul,
								 const FLOAT &taut);
	__device__ FLOAT release(const FLOAT &po, const int &nryr, const FLOAT &cjsr, const FLOAT &cp, FLOAT Jmax);	//SR release current
	__device__ FLOAT lcccurrent(const FLOAT &v, const FLOAT &cp);	//single LCC flux
	__global__ void	setup_kernel(unsigned long long seed,curandStateMRG32k3a *state);	//kernel set up for random number generation
	__global__ void Init(curandStateMRG32k3a *state, FLOAT *randomi, FLOAT *ci,FLOAT *cp,FLOAT *cs,FLOAT *cjsr,FLOAT *cnsr,
			         	 FLOAT *cati,FLOAT *cats,int *lcc, int *nryr, int *nfu, int *ntu, int *nou, int *ncu,int *nob,int *ncb, FLOAT *csp,
			         	 FLOAT *L_temp, FLOAT xratio, int *nsign, int num_RyR);
	__global__ void	Compute(curandStateMRG32k3a *state, int *nl, int *lcc, FLOAT *cp, FLOAT v, 	//updating state variables
							FLOAT dt, FLOAT *xicat, FLOAT *randomi, FLOAT *cjsr, int *nryr, int *nfu,
							int *ntu, int *ncu, int *nou, int *ncb, int *nob, FLOAT *po, FLOAT *xire,
							FLOAT *xinaca, FLOAT *cs, FLOAT xnai, FLOAT *ci, FLOAT *cnsr,
							FLOAT *xips, FLOAT *xisi,FLOAT *xitr, FLOAT *cs_next, FLOAT *ci_next,
							FLOAT *cnsr_next, FLOAT *xiup, FLOAT *xileak, FLOAT *cati, FLOAT *cats,
							FLOAT t, FLOAT tperid, FLOAT *csp, FLOAT dgate1, 
							FLOAT xratio, FLOAT yratio, FLOAT *L_temp, int *nsign, FLOAT *t_on, FLOAT *t_off, FLOAT *t_spark,
							FLOAT svicab, FLOAT CSQbers, FLOAT Jmax, FLOAT TetB);
#endif
